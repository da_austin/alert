import boto3
import logging
from botocore.exceptions import ClientError
from . import settings

logger = logging.getLogger(__name__)

def send_email(sender, recipient, subject, html):

    # Specify a configuration set. If you do not want to use a configuration
    # set, comment the following variable, and the
    # ConfigurationSetName=CONFIGURATION_SET argument below.
    CONFIGURATION_SET = "ConfigSet"

    AWS_REGION = "us-east-1"

    CHARSET = "UTF-8"

    # Create a new SES resource and specify a region.
    client = boto3.client(
        'ses',
        region_name=AWS_REGION,
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
    )
    try:
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    recipient,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': html,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': subject,
                },
            },
            Source=sender,
            # If you are not using a configuration set, comment or delete the
            # following line
            # ConfigurationSetName=CONFIGURATION_SET,
        )
    except ClientError as e:
        logger.error(e.response['Error']['Message'] + ' ' + recipient)
        return 'F', e.response['Error']['Message']
    else:
        logger.info("Email sent! Message ID:" + response['ResponseMetadata']['RequestId'] + ' to ' + recipient)
        return 'S', response['ResponseMetadata']['RequestId']
