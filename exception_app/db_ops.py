from sqlalchemy import *
import logging
import time
import re
import datetime
from exception_app.db import engine,session
from exception_app.models import *
from exception_app.ses_email import *

logger = logging.getLogger(__name__)

def execute_proc(proc_name, params):
    param_str = ','.join(str(x) for x in params)
    statement = "Select * from %s(%s)" % (proc_name, param_str)
    logger.debug(statement)
    records = session.execute(statement)
    return records

def run_exceptions(email_flag, *args):
    metadata = Base.metadata

    # Determine if all or one exception
    if len(args) == 0:
        exceptions = session.query(Exception).all()
        logger.debug('Running all exceptions')
    elif len(args) == 1:
        exceptions = session.query(Exception).filter_by(exception_id=args[0])
        logger.debug('Running single exception ' + str(args[0]))
    else:
        logger.error('Invalid number of arguments of function run_exceptions.')

    # Loop thru exceptions
    for exception in exceptions:
        proc = exception.proc_name
        ex_id = exception.exception_id
        title_parts = exception.title_parts
        primary_field_idx = exception.prim_field_index

        # Internal alerts
        if email_flag == 'I':
            param_set_in = session.query(Param).filter_by(exception_id=ex_id, scope='I').order_by(Param.param_order)
            shortest_param_in = min(param_set_in, key=lambda x:len(x.param_values))
            for x in range(len(shortest_param_in.param_values)):

                email_body = ''
                rowcount = 0
                # dictionary for company_id and html
                html_dict = {}
                last_dt_dict = {}
                color_map_dict = {}
                for company in session.query(Company).filter(Company.exception_flag == 'Y'):
                    list = []
                    comp_id = company.company_id
                    list.append(comp_id)

                    # build subject of email
                    vars = []
                    vals = []
                    for param in param_set_in:
                        list.append(param.param_values[x])
                        vars.append(param.param_name)
                        vals.append(param.param_values[x])
                    subject = construct_subject(x, title_parts, vars, vals)

                    # Run exception function with parameter list
                    results = execute_proc(proc, list)
                    keys = results.keys()
                    rows = results.fetchall()

                    # Get resultset and last insert datetime from last alert
                    last_rs, last_alert_dt = get_last_resultset(ex_id, comp_id, x + 1, 'I')

                    # Compare old and new lists
                    new_rs, color_map = compare_resultsets(rows, last_rs, primary_field_idx)
                    # Create new array in db
                    update_resultset(ex_id, comp_id, x + 1, 'I', new_rs)

                    # Convert result set to html if not empty
                    if results.rowcount > 0:
                        if last_alert_dt != null:
                            last_dt_dict[comp_id] = last_alert_dt
                        html = rs_to_html(company.company_name.upper(), keys, rows, color_map)
                        html_dict[comp_id] = html
                        color_map_dict[comp_id] = color_map

                # Send email alerts and record events
                if html_dict:
                    alertees = session.query(Alertee)\
                        .filter(Alertee.company_id == Company.company_id, Company.company_name == settings.SBD, Alertee.level == (x + 1),
                                Alertee.status == 'Y', ERMap.ex_id == ex_id, ERMap.alert_rcpt_id == ERMap.alert_rcpt_id)
                    for person in alertees:
                        body = merge_html(html_dict, last_dt_dict, color_map_dict, person)
                        if body == '':
                            continue
                        logger.info('Sending email to ' + person.email + ' ' + str(x+1))
                        code, msg = send_email(settings.SENDER, person.email, subject, body)
                        record_alert(settings.SENDER, person.email, ex_id, exception.exc_name, x+1, code, msg)
                        time.sleep(1)

        # External alerts
        if email_flag == 'E':
            param_set_ex = session.query(Param).filter_by(exception_id=ex_id, scope='E').order_by(Param.param_order)
            shortest_param_ex = min(param_set_ex, key=lambda x: len(x.param_values))
            for x in range(len(shortest_param_ex.param_values)):
                for company in session.query(Company).filter(Company.exception_flag == 'Y'):
                    list = []
                    comp_id = company.company_id
                    list.append(comp_id)

                    # build subject of email
                    vars = []
                    vals = []
                    for param in param_set_ex:
                        list.append(param.param_values[x])
                        vars.append(param.param_name)
                        vals.append(param.param_values[x])
                    subject = 'Company test ' + construct_subject(x, title_parts, vars, vals)

                    # Run exception function with parameter list
                    results = execute_proc(proc, list)
                    # Get column names and rows of records
                    keys = results.keys()
                    rows = results.fetchall()

                    # Get resultset and last insert datetime from last alert
                    last_rs, last_alert_dt = get_last_resultset(ex_id, comp_id, x+1, 'E')
                    # Compare old and new lists
                    new_rs, color_map = compare_resultsets(rows, last_rs, primary_field_idx)
                    # Create new array in db
                    update_resultset(ex_id, comp_id, x+1, 'E', new_rs)

                    # Convert result set to html if not empty
                    if len(rows) > 0:
                        html = rs_to_html(company.company_name.upper(), keys, rows, color_map)

                        # Send email alerts and record events
                        alertees = session.query(Alertee)\
                            .filter(Alertee.company_id == comp_id, Alertee.level == (x + 1), Alertee.status == 'Y', ERMap.ex_id == ex_id, ERMap.alert_rcpt_id == Alertee.alert_rcpt_id)
                        for alertee in alertees:
                            email = alertee.email
                            if last_alert_dt != null:
                                if alertee.insert_ts > last_alert_dt:
                                    html_mod = replace_html_style(html)
                                    code, msg = send_email(settings.SENDER, email, subject, html_mod)
                                    logger.info('Sending email to {}, company {}, level {}, exception {}.'.format(email, comp_id, x+1, ex_id))
                                    record_alert(settings.SENDER, email, ex_id, exception.exc_name, x + 1, code, msg)
                                else:
                                    # No alerts if no new records
                                    if 'red' in color_map.values():
                                        code, msg = send_email(settings.SENDER, email, subject, html)
                                        logger.info('Sending email to {}, company {}, level {}, exception {}.'.format(email, comp_id, x + 1, ex_id))
                                        record_alert(settings.SENDER, email, ex_id, exception.exc_name, x + 1, code, msg)
                            else:
                                if 'red' in color_map.values():
                                    html_mod = replace_html_style(html)
                                    code, msg = send_email(settings.SENDER, email, subject, html_mod)
                                    logger.info('Sending email to {}, company {}, level {}, exception {}.'.format(email,
                                                                                                                  comp_id,
                                                                                                                  x + 1,
                                                                                                                  ex_id))
                                    record_alert(settings.SENDER, email, ex_id, exception.exc_name, x + 1, code, msg)


def get_last_resultset(ex_id, comp_id, level, scope):
    last_rs = []
    last_alert_dt = null
    rs_hist = session.query(RSHist).filter_by(exception_id=ex_id, company_id=comp_id, level=level, scope=scope).order_by(desc(RSHist.insert_ts))
    if rs_hist.count() > 0:
        rs_hist = rs_hist.one()
        last_rs = rs_hist.resultset
        if rs_hist.insert_ts:
            if rs_hist.update_ts:
                last_alert_dt = rs_hist.update_ts
            else:
                last_alert_dt = rs_hist.insert_ts
    return last_rs, last_alert_dt

def update_resultset(ex_id, comp_id, level, scope, new_rs):
    rs_hist = session.query(RSHist).filter_by(exception_id=ex_id, company_id=comp_id, level=level, scope=scope).first()
    if not rs_hist:
        rs_hist = RSHist(exception_id = ex_id,
                     company_id = comp_id,
                     level = level,
                     scope = scope,
                     resultset = new_rs,
                     insert_ts = datetime.datetime.now())
        session.add(rs_hist)
        logger.debug('Inserting new resultset: exception {}, company {}, level {}, scope {}'.format(ex_id, comp_id, level, scope))
    else:
        old_rs = rs_hist.resultset
        if set(new_rs).issubset(set(old_rs)):
            logger.debug('Old resultset is same as new one.')
        else:
            logger.debug('Updating resultset: exception {}, company {}, level {}, scope {}'.format(ex_id, comp_id, level, scope))
            rs_hist.resultset = new_rs
        rs_hist.update_ts = datetime.datetime.now()

    session.commit()

def compare_resultsets(records, last_rs, primary_idx):
    new_rs = []
    color_map = {}
    idx = 0
    for record in records:
        primary = record[primary_idx - 1]
        new_rs.append(primary)
        if primary not in last_rs:
            color_map[idx] = 'red'
        else:
            color_map[idx] = 'black'
        idx += 1
    return new_rs, color_map

def record_alert(sender, recipient, ex_id, subject, level, code, msg):
    current_ts = datetime.datetime.now()
    alt = Alert(send_from = sender,
                send_to = recipient,
                exception_id = ex_id,
                subject = subject,
                level = level,
                status = code,
                msg = msg,
                time_sent = current_ts)
    session.add(alt)
    session.commit()

def construct_subject(level, names, vars, vals):
    subject = 'Alert Level ' + str(level+1) + ' -- '
    # merge vars and vals first
    params = []
    for i in range(len(vars)):
        params.append(str(vals[i]) + ' ' + vars[i])

    len1 = len(names)
    len2 = len(params)
    merged = []

    for i in range(len1):
        merged.append(names[i])
        if i < len2:
            merged.append(params[i])
    for j in range(i+1, len2):
        merged.append(params[j])

    subject += ' '.join(merged)
    return subject

def rs_to_html(header, keys, rows, color_map):
    html_str = """<html>
    <head></head>
    <body>
      <h1>"""
    html_str += header
    html_str += """</h1>
      <table border="1" align="center">
          <tr>"""
    for key in keys:
        html_str += "<th>" + format_col_name(key) + "</th>"
    html_str += "</tr>\n"
    for i, row in enumerate(rows):
        html_str += "<tr>"
        for col in row:
            if color_map[i] == 'red':
                html_str += '<td align=center><span style="color: red;">' + str(col) + "</span></td>"
            else:
                html_str += '<td align=center>' + str(col) + "</td>"
        html_str += "</tr>"

    html_str += """
      </table>
    </body>
    </html>
    """
    return html_str

def replace_html_style(html):
    new_html = re.sub('color: red', 'color: black', html)
    return new_html

def format_col_name(name):
    if name == 'svc_ord_nbr':
        return 'SVC_ORD #'
    if name == 'po_nbr':
        return 'PO #'
    adverbs = ['a', 'an', 'of', 'per', 'the', 'to']
    items = name.split('_')
    col = ''
    for s in items:
        if not s in adverbs:
            s = s.title()
        if s == 'Number' or s == 'No' or s == 'Nbr':
            s = '#'
        elif s == 'per':
            s = '/'
        if s == '/' or col.endswith('/'):
            col += s
        else:
            col += ' ' + s
    return col.strip()


#build message body by looking comp_id-html dict and Alertee object
def merge_html(html_dict, dt_dict, color_map_dict, rcpt):
    body = ''
    for c in rcpt.comp_list:
        if c in html_dict:
            html = html_dict[c]
            if c in dt_dict:
                if rcpt.insert_ts >= dt_dict[c]:
                    html = replace_html_style(html)
                else:
                    if 'red' not in color_map_dict[c].values():
                        continue
            else:
                html = replace_html_style(html)
            body += html
    return body



