from sqlalchemy.ext.declarative import declarative_base, DeferredReflection
from exception_app.db import engine,session

Base = declarative_base(engine)

class Company(Base):
    __tablename__ = 'company'
    __table_args__ = {'autoload': True}

class Exception(Base):
    __tablename__ = 'exception'
    __table_args__ = {'autoload': True}

class Param(Base):
    __tablename__ = 'exc_param'
    __table_args__ = {'autoload': True}

class Alertee(Base):
    __tablename__ = 'alert_rcpt'
    __table_args__ = {'autoload': True}

class Alert(Base):
    __tablename__ = 'alert'
    __table_args__ = {'autoload': True}

class ERMap(Base):
    __tablename__ = 'exc_rcpt_map'
    __table_args__ = {'autoload': True}

class RSHist(Base):
    __tablename__ = 'exc_resultset_hist'
    __table_args__ = {'autoload': True}