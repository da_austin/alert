from sqlalchemy import exc
import sys
from exception_app.db import engine,session
import datetime
from exception_app.models import *

def add_emails_from_file(csv_file):
    row_id = 0
    with open(csv_file) as file:
        for line in file:
            row_id += 1
            if row_id == 1:
                continue
            items = line.rstrip().split('\t')
            if len(items) < 4:
                continue
            email = items[0]
            company = items[1]
            level = items[2]
            comp_list = items[3]
            if '@' in email and isInt(company) and isInt(level) and isCompList(comp_list):
                pid = add_person('', email, company, level, [int(x) for x in comp_list.split(',')])
                if pid > 0:
                    eids = [e.exception_id for e in session.query(Exception.exception_id).all()]
                    add_epmap(pid, eids)

def add_person(name, email, company, level, comp_list):
    current_ts = datetime.datetime.now()
    p = Person(name = name,
               email = email,
               company_id = company,
               level = level,
               comp_list = comp_list,
               status = 'Y',
               insert_ts = current_ts)
    session.add(p)
    try:
        session.commit()
        session.refresh(p)
        print(p.person_id)
        return p.person_id
    except exc.SQLAlchemyError:
        sys.exit("SQLAlchemy commit error: can't insert person")

def add_epmap(pid, eids):
    current_ts = datetime.datetime.now()
    for eid in eids:
        p_e = EPMap(ex_id = eid,
                    person_id = pid,
                    insert_ts = current_ts)
        session.add(p_e)
    try:
        session.commit()
    except exc.SQLAlchemyError:
        sys.exit("SQLAlchemy commit error: can't insert into EPMap")

def isInt(num):
    try:
        int(num)
        return True
    except:
        return False

def isCompList(s):
    if not isInt(s):
        comps = s.split(',')
        if len(comps) == 1:
            return False
        for c in comps:
            if not isInt(c):
                return False
    return True

if __name__ == '__main__':
    file_path = "C:/Users/dnj0109/Work/SmartDoor/Exception/alertees.txt"
    add_emails_from_file(file_path)
