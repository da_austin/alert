from sqlalchemy import *
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine.url import URL
import logging
from . import settings


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

engine = create_engine(URL(**settings.DATABASE_PROD))
session = scoped_session(sessionmaker(bind=engine))
