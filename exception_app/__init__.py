import os
import logging
import logging.config
import yaml

config_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logging.yaml')
if os.path.exists(config_path):
    with open(config_path, 'rt') as f:
        config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)
else:
    logging.basicConfig(level=logging.INFO)

# Log that the logger was configured
logger = logging.getLogger(__name__)
logger.info('Completed configuring logger()!')