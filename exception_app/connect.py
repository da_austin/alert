import sys
import logging
import psycopg2

# rds settings
rds_host = "sbd-nexus.cqf2t29nf7qf.us-east-2.rds.amazonaws.com"
port = '5432'
name = 'door'
password = 'stanley123'
db_name = 'smartdoor_dev'


conn = psycopg2.connect(host=rds_host, port=port, user=name, password=password, database=db_name)
# except:
#     print("ERROR: Unexpected error: Could not connect to  instance.")
#     sys.exit()

print("SUCCESS: Connection to RDS  instance succeeded")


# def handler(event, context):
#     """
#     This function fetches content from mysql RDS instance
#     """
#
#     item_count = 0
#
#     with conn.cursor() as cur:
#         cur.execute("create table Employee3 ( EmpID  int NOT NULL, Name varchar(255) NOT NULL, PRIMARY KEY (EmpID))")
#         cur.execute('insert into Employee3 (EmpID, Name) values(1, "Joe")')
#         cur.execute('insert into Employee3 (EmpID, Name) values(2, "Bob")')
#         cur.execute('insert into Employee3 (EmpID, Name) values(3, "Mary")')
#         conn.commit()
#         cur.execute("select * from Employee3")
#         for row in cur:
#             item_count += 1
#             logger.info(row)
#             # print(row)
#
#     return "Added %d items from RDS MySQL table" % (item_count)