import unittest
from exception_app.db import engine,session
from exception_app.models import *
from exception_app import db_ops

@unittest.skip("classing skipping")
class TestExecuteProc(unittest.TestCase):

    def test(self):
        list = [1, 3, 12]
        records = db_ops.execute_proc("most_service_calls", list)
        print(*records, sep = '\n')

@unittest.skip("classing skipping")
class TestRunJobs(unittest.TestCase):
    def test(self):
        db_ops.run_exceptions('I', 3)

@unittest.skip("classing skipping")
class TestRecordAlert(unittest.TestCase):
    def test(self):
        db_ops.record_alert('x', 'y', 1, 's', 1, 'S', 'xxx001')

@unittest.skip("classing skipping")
class TestConstructSubject(unittest.TestCase):
    def test(self):
        names = ['Tickets Open Beyond', 'in the last']
        vars = ['Days', 'months']
        vals = [23, 3]
        self.assertEqual(db_ops.construct_subject(1, names, vars, vals), 'Alert Level 2 -- Tickets Open Beyond 23 Days in the last 3 months')


@unittest.skip("classing skipping")
class TestFormatColNmae(unittest.TestCase):
    def test(self):
        name1 = "number_of_service_calls"
        self.assertEqual(db_ops.format_col_name(name1), '# of Service Calls')
        name2 = 'cost_per_call'
        print(db_ops.format_col_name(name2))
        self.assertEqual(db_ops.format_col_name(name2), 'Cost/Call')

#@unittest.skip("classing skipping")
class TestGetLastResultSet(unittest.TestCase):
    def test(self):
        [print(x) for x in db_ops.get_last_resultset(1,3,2,'E')]

@unittest.skip("classing skipping")
class UpdateResultSet(unittest.TestCase):
    def test(self):
        new_rs = ['WALMART 005', 'WALMART 016']
        db_ops.update_resultset(1, 3, 2, 'E', new_rs)

@unittest.skip("classing skipping")
class TestQueryAlertee(unittest.TestCase):
    def test(self):
        alertees = session.query(Alertee) \
            .filter(Alertee.company_id == Company.company_id, Company.company_name == 'LOWES',
                    Alertee.level == 1,
                    Alertee.status == 'Y', ERMap.ex_id == 50, ERMap.alert_rcpt_id == ERMap.alert_rcpt_id)
        alertees = session.query(Alertee) \
            .filter(Alertee.company_id == 1, Alertee.level == 1, Alertee.status == 'Y',
                    ERMap.ex_id == 4, ERMap.alert_rcpt_id == Alertee.alert_rcpt_id)
        for p in alertees:
            print('company_id', p.company_id)

if __name__ == '__main__':
    unittest.main()


