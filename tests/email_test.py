import unittest

from exception_app import ses_email

class TestSendEmail(unittest.TestCase):

    def test(self):
        sender = "S-SAT-VirtualAssistant@sbdinc.com"
        emails = ['dian.jiao@sbdinc.com']
        for recv in emails:
            code, msg = ses_email.send_email(sender, recv, "alert test", "alert test")
            print(recv, ' ', code, "  ", msg)


if __name__ == '__main__':
    unittest.main()